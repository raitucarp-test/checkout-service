FROM golang
ADD . ./

RUN go mod download
ENTRYPOINT /go/cmd/checkout-service
EXPOSE 8080