package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"reflect"
	"testing"

	"bitbucket.org/raitucarp-test/checkout-service/api"
	"bitbucket.org/raitucarp-test/checkout-service/internal/message"
	"bitbucket.org/raitucarp-test/checkout-service/internal/model"
	"github.com/google/jsonapi"
	"github.com/stretchr/testify/assert"
)

type TestRequestData struct {
	apiVersion string
	method     string
	url        string
	body       io.Reader
}

type TestRequestDataWithAuth struct {
	apiVersion string
	method     string
	url        string
	body       io.Reader
	username   string
	password   string
}

func basicAuth(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

func redirectPolicyFunc(req *http.Request, via []*http.Request) error {
	req.Header.Add("Authorization", "Basic "+basicAuth("username1", "password123"))
	return nil
}

func mockRequest(rd TestRequestData) (*httptest.ResponseRecorder, error) {
	router = api.UseRouter(rd.apiVersion)
	w := httptest.NewRecorder()
	request, err := http.NewRequest(rd.method, rd.url, rd.body)
	if err != nil {
		return w, err
	}
	router.ServeHTTP(w, request)
	return w, nil
}

func mockRequestWithAuth(rd TestRequestDataWithAuth) (*httptest.ResponseRecorder, error) {
	router = api.UseRouter(rd.apiVersion)
	w := httptest.NewRecorder()
	request, err := http.NewRequest(rd.method, rd.url, rd.body)
	if err != nil {
		return w, err
	}

	request.Header.Add("Authorization", "Basic "+basicAuth(rd.username, rd.password))
	router.ServeHTTP(w, request)
	return w, nil
}

func TestIndexPage(t *testing.T) {
	resp, _ := mockRequest(TestRequestData{
		apiVersion: "v1",
		method:     "GET",
		url:        "/",
		body:       nil,
	})

	msg := &message.WelcomeMessage{}
	err := jsonapi.UnmarshalPayload(resp.Body, msg)
	if err != nil {
		assert.Fail(t, err.Error())
	}

	assert.Equal(t, http.StatusOK, resp.Code, "Status code should be OK")
	assert.Equal(t, "", msg.MessageType, "Message type is not equal")
	assert.Equal(t, "Welcome to Checkout service", msg.Message, "Welcome message should equal")
}

func Test404NotFound(t *testing.T) {
	resp, _ := mockRequest(TestRequestData{
		apiVersion: "v1",
		method:     "GET",
		url:        "/items",
		body:       nil,
	})

	msg := &message.ErrorMessage{}
	err := jsonapi.UnmarshalPayload(resp.Body, msg)
	if err != nil {
		assert.Fail(t, err.Error())
	}

	assert.Equal(t, http.StatusNotFound, resp.Code, "Status code should be OK")
	assert.Equal(t, "NOT_FOUND", msg.ErrorCode, "Error code should equal")
	assert.Equal(t, "Endpoint not found", msg.Message, "404 message should equal")
}

func TestGetAllItems(t *testing.T) {
	resp, _ := mockRequest(TestRequestData{
		apiVersion: "v1",
		method:     "GET",
		url:        "/v1/items/",
		body:       nil,
	})

	items, err := jsonapi.UnmarshalManyPayload(resp.Body, reflect.TypeOf(new(model.Item)))
	if err != nil {
		assert.Fail(t, err.Error())
	}

	assert.Equal(t, http.StatusOK, resp.Code, "Status code should be OK")
	assert.Equal(t, 4, len(items), "Item length should be 4")
}

func TestAddNewItemSuccess(t *testing.T) {
	newItem := &model.Item{
		SKU:   "9I3BJK",
		Name:  "Monitor",
		Price: 400.39,
	}

	newItemJson, _ := json.Marshal(newItem)
	resp, _ := mockRequest(TestRequestData{
		apiVersion: "v1",
		method:     "POST",
		url:        "/v1/items/",
		body:       bytes.NewBuffer(newItemJson),
	})

	assert.Equal(t, http.StatusUnauthorized, resp.Code, "Status code should be Unauthorized")

	resp, _ = mockRequestWithAuth(TestRequestDataWithAuth{
		apiVersion: "v1",
		method:     "POST",
		url:        "/v1/items/",
		username:   os.Getenv("BASIC_AUTH_USERNAME"),
		password:   os.Getenv("BASIC_AUTH_PASSWORD"),
		body:       bytes.NewBuffer(newItemJson),
	})

	assert.Equal(t, http.StatusOK, resp.Code, "Status code should be OK")

	resp, _ = mockRequest(TestRequestData{
		apiVersion: "v1",
		method:     "GET",
		url:        "/v1/items/",
		body:       nil,
	})

	items, err := jsonapi.UnmarshalManyPayload(resp.Body, reflect.TypeOf(new(model.Item)))
	if err != nil {
		assert.Fail(t, err.Error())
	}

	assert.Equal(t, http.StatusOK, resp.Code, "Status code should be OK")
	assert.Equal(t, 5, len(items), "Item length should be 5")
}
