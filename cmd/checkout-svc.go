package main

import (
	"flag"
	"net/http"
	"os"

	"bitbucket.org/raitucarp-test/checkout-service/api"
	"bitbucket.org/raitucarp-test/checkout-service/internal/data"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

var router *gin.Engine

func loadEnvironments() {
	if flag.Lookup("test.v") == nil {
		godotenv.Load("../.env")
	} else {
		godotenv.Load()
	}
}

func init() {
	loadEnvironments()
	data.PopulateFirstData()
	router = api.UseRouter("v1")
}

func main() {
	port := os.Getenv("PORT")
	http.ListenAndServe(":"+port, router)
}
