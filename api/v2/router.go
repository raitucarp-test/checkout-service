package v2

import "github.com/gin-gonic/gin"

func CreateRouter(router *gin.Engine) *gin.Engine {
	v1 := router.Group("/v1")
	{
		itemsRoute := v1.Group("/items")
		{
			itemsRoute.GET("/")
		}

		// v1.POST("/login", loginEndpoint)
		// v1.POST("/submit", submitEndpoint)
		// v1.POST("/read", readEndpoint)
	}

	return router
}
