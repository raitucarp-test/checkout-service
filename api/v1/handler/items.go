package handler

import (
	"fmt"
	"net/http"

	"bitbucket.org/raitucarp-test/checkout-service/internal/data"
	"bitbucket.org/raitucarp-test/checkout-service/internal/message"
	"bitbucket.org/raitucarp-test/checkout-service/internal/model"
	"github.com/gin-gonic/gin"
	"github.com/google/jsonapi"
	"github.com/google/uuid"
)

func ListAllItems(c *gin.Context) {
	result, err := jsonapi.Marshal(data.Items)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &message.ErrorMessage{Message: err.Error()})
		return
	}
	c.JSON(http.StatusOK, result)
}

func AddItemBySKU(c *gin.Context) {
	var item model.Item
	if err := c.ShouldBindJSON(&item); err != nil {
		c.JSON(http.StatusBadRequest, message.NewErrorMessage("BAD_REQUEST", err.Error()))
		return
	}

	duplicateItem := false
	for _, _item := range data.Items {
		if _item.SKU == item.SKU {
			duplicateItem = true
		}
	}

	if duplicateItem {
		c.JSON(http.StatusNotFound, message.NewErrorMessage("BAD_REQUEST", fmt.Sprintf("Item %s duplicate", item.SKU)))
		return
	}

	item.UUID = uuid.NewString()
	data.AddItem(&item)
	c.JSON(http.StatusOK, message.NewSuccessMessage(fmt.Sprintf("Item %s successfully added", item.SKU)))
}

func GetItemBySKU(c *gin.Context) {
	sku := c.Param("sku")
	foundItem := data.Items.FindItemBySKU(sku)

	if foundItem == nil {
		c.JSON(http.StatusNotFound, message.NewErrorMessage("ITEM_NOT_FOUND", fmt.Sprintf("Item %s not found", sku)))
		return
	}

	result, _ := jsonapi.Marshal(foundItem)
	c.JSON(http.StatusOK, result)
}

func UpdateItemBySKU(c *gin.Context) {
	var item model.Item
	if err := c.ShouldBindJSON(&item); err != nil {
		c.JSON(http.StatusBadRequest, message.NewErrorMessage("BAD_REQUEST", err.Error()))
		return
	}

	SKU := c.Param("sku")
	foundItem := data.Items.FindItemBySKU(SKU)

	if foundItem == nil {
		c.JSON(http.StatusNotFound, message.NewErrorMessage("ITEM_NOT_FOUND", fmt.Sprintf("Item %s not found", SKU)))
		return
	}

	data.Items.UpdateItemBySKU(SKU, &item)
	c.JSON(http.StatusOK, message.NewSuccessMessage(fmt.Sprintf("Item %s successfully updated", SKU)))
}

func DeleteItemBySku(c *gin.Context) {
	SKU := c.Param("sku")
	foundItem := data.Items.FindItemBySKU(SKU)

	if foundItem == nil {
		c.JSON(http.StatusNotFound, message.NewErrorMessage("ITEM_NOT_FOUND", fmt.Sprintf("Item %s not found", SKU)))
		return
	}

	for index, item := range data.Items {
		if item.SKU == SKU {
			data.Items = append(data.Items[:index], data.Items[index+1:]...)
		}
	}

	c.JSON(http.StatusOK, message.NewSuccessMessage(fmt.Sprintf("Item %s successfully deleted from items", SKU)))
}
