package handler

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"bitbucket.org/raitucarp-test/checkout-service/internal/data"
	"bitbucket.org/raitucarp-test/checkout-service/internal/message"
	"bitbucket.org/raitucarp-test/checkout-service/internal/model"
	"github.com/gin-gonic/gin"
	"github.com/google/jsonapi"
	"gopkg.in/yaml.v2"
)

func ListAllRules(c *gin.Context) {
	result, err := jsonapi.Marshal(data.Rules)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &message.ErrorMessage{Message: err.Error()})
		return
	}
	c.JSON(http.StatusOK, result)
}

func ListRawRules(c *gin.Context) {
	var rawRules []string
	for _, rawRule := range data.Rules {
		rawRules = append(rawRules, rawRule.Raw)
	}

	c.String(200, strings.Join(rawRules, "\n\n"))

}

func AddRule(c *gin.Context) {
	rule := &model.Rule{}
	yamlData, err := ioutil.ReadAll(c.Request.Body)

	err = yaml.Unmarshal(yamlData, &rule)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &message.ErrorMessage{Message: err.Error()})
		return
	}

	var foundRule *model.Rule
	for _, rawRule := range data.Rules {
		if rawRule.Name == rule.Name {
			foundRule = rawRule
		}
	}

	if foundRule != nil {
		c.JSON(http.StatusBadRequest, message.NewErrorMessage("DUPLICATE_RULE", fmt.Sprintf("Rule %s already exist cannot be duplicated", rule.Name)))
		return
	}

	data.Rules = append(data.Rules, rule)
	c.JSON(http.StatusOK, message.NewSuccessMessage(fmt.Sprintf("Rule %s successfully added", rule.Name)))
}

func RemoveRuleByName(c *gin.Context) {
	var foundRule *model.Rule
	name := c.Param("name")
	for _, rawRule := range data.Rules {
		if rawRule.Name == name {
			foundRule = rawRule
		}
	}

	if foundRule == nil {
		c.JSON(http.StatusNotFound, message.NewErrorMessage("RULE_NOT_FOUND", fmt.Sprintf("Rule %s not found", name)))
		return
	}

	for index, rawRule := range data.Rules {
		if rawRule.Name == name {
			data.Rules = append(data.Rules[:index], data.Rules[index+1:]...)
		}
	}
	c.JSON(http.StatusOK, message.NewSuccessMessage(fmt.Sprintf("Rule %s successfully deleted from items", name)))
}

func GetRuleByName(c *gin.Context) {

}
