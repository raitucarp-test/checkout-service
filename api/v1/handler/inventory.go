package handler

import (
	"fmt"
	"net/http"

	"bitbucket.org/raitucarp-test/checkout-service/internal/data"
	"bitbucket.org/raitucarp-test/checkout-service/internal/message"
	"github.com/gin-gonic/gin"
	"github.com/google/jsonapi"
)

func ListItemsInInventory(c *gin.Context) {
	result, err := jsonapi.Marshal(data.Inventory)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &message.ErrorMessage{Message: err.Error()})
		return
	}
	c.JSON(http.StatusOK, result)
}

func AddItemToInventory(c *gin.Context) {
	var inventoryItem data.AddItemToInventoryRequestPayload
	if err := c.ShouldBindJSON(&inventoryItem); err != nil {
		c.JSON(http.StatusBadRequest, message.NewErrorMessage("BAD_REQUEST", err.Error()))
		return
	}

	sku := inventoryItem.SKU
	foundItem := data.Items.FindItemBySKU(sku)
	itemInInventory := data.Inventory.FindItemBySKU(sku)

	if foundItem == nil {
		c.JSON(http.StatusNotFound, message.NewErrorMessage("ITEM_NOT_FOUND", fmt.Sprintf("Item %s not found", sku)))
		return
	}

	if itemInInventory == nil {
		data.Inventory.AddItemToInventory(foundItem, inventoryItem.Quantity)
		c.JSON(http.StatusOK, message.NewSuccessMessage(fmt.Sprintf("Item %s successfully added to inventory", sku)))
		return
	}

	data.Inventory.UpdateItemQuantity(sku, itemInInventory.Quantity+inventoryItem.Quantity)
	c.JSON(http.StatusOK, message.NewSuccessMessage(fmt.Sprintf("Item %s successfully updated its quantit", sku)))
	return
}

func RemoveItemFromInventory(c *gin.Context) {
	sku := c.Param("sku")
	itemInInventory := data.Inventory.FindItemBySKU(sku)

	if itemInInventory == nil {
		c.JSON(http.StatusNotFound, message.NewErrorMessage("ITEM_INVENTORY_NOT_FOUND", fmt.Sprintf("Item %s not found in inventory", sku)))
		return
	}

	data.Inventory.RemoveItemFromInventory(sku)
	c.JSON(http.StatusOK, message.NewSuccessMessage(fmt.Sprintf("Item %s successfully removed from inventory", sku)))
	return
}
