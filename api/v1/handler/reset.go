package handler

import (
	"bitbucket.org/raitucarp-test/checkout-service/internal/data"
	"bitbucket.org/raitucarp-test/checkout-service/internal/message"
	"github.com/gin-gonic/gin"
	"github.com/google/jsonapi"
)

func ResetData(c *gin.Context) {
	data.ResetItemsAndInventory()
	c.Status(200)
	jsonapi.MarshalPayload(c.Writer, &message.SuccessOpMessage{Message: "Success reset data"})
}
