package handler

import (
	"fmt"
	"net/http"

	"bitbucket.org/raitucarp-test/checkout-service/internal/data"
	"bitbucket.org/raitucarp-test/checkout-service/internal/message"
	"bitbucket.org/raitucarp-test/checkout-service/internal/model"
	"github.com/gin-gonic/gin"
	"github.com/google/jsonapi"
)

func ListAllItemInCart(c *gin.Context) {
	result, err := jsonapi.Marshal(data.Cart)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &message.ErrorMessage{Message: err.Error()})
		return
	}
	c.JSON(http.StatusOK, result)
}

func AddItemToCart(c *gin.Context) {
	var cartItem data.AddItemToCartRequestPayload
	if err := c.ShouldBindJSON(&cartItem); err != nil {
		c.JSON(http.StatusBadRequest, message.NewErrorMessage("BAD_REQUEST", err.Error()))
		return
	}

	foundItem := model.CartItem{}
	itemAvailable := false
	itemOutOfStock := false
	for _, itemInventory := range data.Inventory {
		if itemInventory.Item.SKU == cartItem.SKU {
			itemAvailable = true
			foundItem = model.CartItem{
				Item: &model.Item{
					UUID:     itemInventory.Item.UUID,
					SKU:      itemInventory.Item.SKU,
					Name:     itemInventory.Item.Name,
					Price:    itemInventory.Item.Price,
					Currency: itemInventory.Item.Currency,
				},
				DiscountPercentage: 0,
				Free:               false,
				FreeItems:          []string{},
			}
			if itemInventory.Quantity <= 0 {
				itemOutOfStock = true
			}
		}
	}

	if !itemAvailable {
		c.JSON(http.StatusBadRequest, message.NewErrorMessage("ITEM_EMPTY", "Item not in inventory"))
		return
	}

	if itemOutOfStock {
		c.JSON(http.StatusBadRequest, message.NewErrorMessage("ITEM_EMPTY", "Item out of stock"))
		return
	}

	data.Cart.AddItemToCart(foundItem, cartItem.Quantity)
	data.Cart.CheckItemsAvailibility(&data.Inventory)
	data.Cart.CalculatePotentialPromo()
	data.Cart.CheckFreeItemsInCart()
	data.Cart.RecalculateTotalPrice()
	c.JSON(http.StatusOK, message.NewSuccessMessage(fmt.Sprintf("Item %s successfully updated its quantity", cartItem.SKU)))
	return
}

func RemoveItemInCartBySKU(c *gin.Context) {
	var removeItemInCartPayload data.RemoveItemInCartRequestPayload
	if err := c.ShouldBindJSON(&removeItemInCartPayload); err != nil {
		c.JSON(http.StatusBadRequest, message.NewErrorMessage("BAD_FORMAT", err.Error()))
		return
	}

	removeQuota := removeItemInCartPayload.Quantity

	sku := c.Param("sku")

	for index, cartItem := range data.Cart.Items {
		if cartItem.SKU == sku && removeQuota > 0 {
			data.Cart.Items = append(data.Cart.Items[:index], data.Cart.Items[index+1:]...)
			removeQuota--
		}
	}

	data.Cart.CheckItemsAvailibility(&data.Inventory)
	data.Cart.CalculatePotentialPromo()
	data.Cart.CheckFreeItemsInCart()
	data.Cart.RecalculateTotalPrice()

	if removeQuota != 0 {
		c.JSON(http.StatusBadRequest, message.NewErrorMessage("CART_ITEM_NOT_FOUND", "Cannot remove item in cart. Probably not in cart"))
		return
	}

	c.JSON(http.StatusOK, message.NewSuccessMessage(fmt.Sprintf("Successfully remove %d %s in cart", removeItemInCartPayload.Quantity, sku)))
	return
}

func ClearCart(c *gin.Context) {
	if len(data.Cart.Items) <= 0 {
		c.JSON(http.StatusBadRequest, message.NewErrorMessage("CART_IS_EMPTY", "Cannot clear cart"))
		return
	}

	data.Cart.Items = []*model.CartItem{}
	c.JSON(http.StatusOK, message.NewSuccessMessage("Successfully clear cart"))
}

func CheckoutCart(c *gin.Context) {
	if len(data.Cart.Items) <= 0 {
		c.JSON(http.StatusBadRequest, message.NewErrorMessage("CART_IS_EMPTY", "Cannot checkout cart is empty"))
		return
	}

	anyUnavailableItem := []string{}
	for _, cartItem := range data.Cart.Items {
		if cartItem.Available == false {
			anyUnavailableItem = append(anyUnavailableItem, cartItem.SKU)
		}
	}

	if len(anyUnavailableItem) > 0 {
		c.JSON(http.StatusBadRequest, message.NewErrorMessage("ANY_UNAVAILABLE_ITEM", "There is unavailable item", anyUnavailableItem))
		return
	}

	for _, cartItem := range data.Cart.Items {
		for _, inventory := range data.Inventory {
			if cartItem.SKU != inventory.Item.SKU {
				continue
			}

			if inventory.Quantity > 0 {
				inventory.Quantity--
			}
		}

		data.Cart.Items = []*model.CartItem{}
		c.JSON(http.StatusOK, message.NewSuccessMessage("Successfully checkout"))
	}
}
