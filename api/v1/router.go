package api

import (
	"bitbucket.org/raitucarp-test/checkout-service/api/v1/handler"
	"bitbucket.org/raitucarp-test/checkout-service/internal/auth"
	"github.com/gin-gonic/gin"
)

func CreateRouter(router *gin.Engine) {
	useAuth := auth.CheckoutBasicAuth()

	v1 := router.Group("/v1")
	{
		v1.POST("/reset", useAuth, handler.ResetData)

		itemsRoute := v1.Group("/items")
		{
			itemsRoute.GET("/", handler.ListAllItems)
			itemsRoute.POST("/", useAuth, handler.AddItemBySKU)
		}

		itemRoute := v1.Group("/item")
		{
			itemRoute.GET("/:sku", handler.GetItemBySKU)
			itemRoute.PUT("/:sku", useAuth, handler.UpdateItemBySKU)
			itemRoute.DELETE("/:sku", useAuth, handler.DeleteItemBySku)
		}

		inventoryRoute := v1.Group("/inventory")
		{
			inventoryRoute.GET("/", handler.ListItemsInInventory)
			inventoryRoute.POST("/", useAuth, handler.AddItemToInventory)
			inventoryRoute.DELETE("/:sku", useAuth, handler.RemoveItemFromInventory)
		}

		rulesRoute := v1.Group("/rules")
		{
			rulesRoute.GET("/", useAuth, handler.ListAllRules)
			rulesRoute.GET("/raw", useAuth, handler.ListRawRules)
			rulesRoute.POST("/", useAuth, handler.AddRule)
		}

		ruleRoute := v1.Group("/rule")
		{
			ruleRoute.GET("/:name", useAuth, handler.GetRuleByName)
			ruleRoute.DELETE("/:name", useAuth, handler.RemoveRuleByName)
		}

		cartRoute := v1.Group("/cart")
		{
			cartRoute.GET("/", handler.ListAllItemInCart)
			cartRoute.POST("/", handler.AddItemToCart)
			cartRoute.DELETE("/:sku", handler.RemoveItemInCartBySKU)
			cartRoute.POST("/clear", handler.ClearCart)
			cartRoute.POST("/checkout", handler.CheckoutCart)
		}
	}
}
