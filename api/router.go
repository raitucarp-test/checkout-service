package api

import (
	"net/http"

	"github.com/gin-gonic/gin"

	v1 "bitbucket.org/raitucarp-test/checkout-service/api/v1"
	"bitbucket.org/raitucarp-test/checkout-service/internal/message"
)

func UseRouter(version ...string) *gin.Engine {
	router := gin.Default()
	apiVersions := map[string]func(*gin.Engine){
		"v1": v1.CreateRouter,
	}

	for _, v := range version {
		for apiVersion, fn := range apiVersions {
			if v == apiVersion {
				fn(router)
			}
		}
	}

	router.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, message.NewWelcomeMessage("Welcome to Checkout service"))
	})

	router.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusNotFound, message.NewErrorMessage("NOT_FOUND", "Endpoint not found"))
	})

	return router
}
