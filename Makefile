build-app:
	echo "build"
	go build -o checkout-app ./cmd/checkout-svc.go 

build-run:
	go build -o checkout-app ./cmd/checkout-svc.go
	./checkout-app

run:
	go run ./cmd/checkout-svc.go

run-test:
	go test -v -coverprofile cover.out ./cmd
	go tool cover -html=cover.out -o cover.html