# Checkout Service Demo

This is Checkout Service demo with golang

## To build and run

```
make build-run
```

## Environment Variable

```
BASIC_AUTH_USERNAME=demo
BASIC_AUTH_PASSWORD=password
PORT=8080
BASE_URL=http://localhost:$PORT/v1
```
