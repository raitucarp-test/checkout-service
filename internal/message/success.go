package message

import "github.com/google/jsonapi"

type SuccessOpMessage struct {
	MessageType string `jsonapi:"primary,success_message"`
	Message     string `jsonapi:"attr,message"`
}

func NewSuccessMessage(message string) jsonapi.Payloader {
	result, err := jsonapi.Marshal(&SuccessOpMessage{Message: message})
	if err != nil {
		panic(err)
	}
	return result
}
