package message

import "github.com/google/jsonapi"

type ErrorMessage struct {
	MessageType string      `jsonapi:"primary,error_message"`
	ErrorCode   string      `jsonapi:"attr,error_code"`
	Message     string      `jsonapi:"attr,message"`
	Extra       interface{} `jsonapi:"attr,extra"`
}

func NewErrorMessage(errorCode string, message string, extra ...interface{}) jsonapi.Payloader {
	result, err := jsonapi.Marshal(&ErrorMessage{ErrorCode: errorCode, Message: message, Extra: extra})
	if err != nil {
		panic(err)
	}
	return result
}
