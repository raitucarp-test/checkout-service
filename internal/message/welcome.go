package message

import "github.com/google/jsonapi"

type WelcomeMessage struct {
	MessageType string `jsonapi:"primary,welcome_message"`
	Message     string `jsonapi:"attr,message"`
}

func NewWelcomeMessage(message string) jsonapi.Payloader {
	result, err := jsonapi.Marshal(&WelcomeMessage{Message: message})
	if err != nil {
		panic(err)
	}
	return result
}
