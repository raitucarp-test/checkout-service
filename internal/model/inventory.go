package model

import (
	"fmt"

	"github.com/google/uuid"
	"github.com/leekchan/accounting"
)

type InventoryItem struct {
	ID       string `jsonapi:"primary,inventory"`
	Item     *Item  `jsonapi:"relation,item"`
	Quantity int    `jsonapi:"attr,quantity"`
}

type Inventory []*InventoryItem

func NewInventory() (inv Inventory) {
	return
}

func (inv *Inventory) AddItemToInventory(item *Item, qty int) {
	*inv = append(*inv, &InventoryItem{ID: uuid.NewString(), Item: item, Quantity: qty})
}

func (inv *Inventory) UpdateItemQuantity(SKU string, qty int) {
	for _, invItem := range *inv {
		if invItem.Item.SKU == SKU {
			invItem.Quantity = qty
		}
	}
}

func (inv *Inventory) FindItemBySKU(SKU string) *InventoryItem {
	var foundItem *InventoryItem

	for _, inventory := range *inv {
		if inventory.Item.SKU == SKU {
			foundItem = inventory
		}
	}

	return foundItem
}

func (inv *Inventory) RemoveItemFromInventory(SKU string) {
	for index, invItem := range *inv {
		if invItem.Item.SKU == SKU {
			*inv = append((*inv)[:index], (*inv)[index+1:]...)
		}
	}
}

func (inv Inventory) LogAllItems(tag string) {
	logTag := "[" + tag + "]"

	for _, invItem := range inv {
		item := invItem.Item
		ac := accounting.Accounting{Symbol: item.Currency, Precision: 2}
		fmt.Println(logTag, "---")
		fmt.Println(logTag, "Item SKU: ", item.SKU)
		fmt.Println(logTag, "Item name:", item.Name)
		fmt.Println(logTag, "Item price:", ac.FormatMoney(item.Price))
	}
}
