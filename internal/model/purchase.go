package model

// type PurchasedItem struct {
// 	ID         uint           `json:"id" gorm:"primaryKey;autoIncrement:true"`
// 	CreatedAt  time.Time      `json:"created_at"`
// 	UpdatedAt  time.Time      `json:"updated_at"`
// 	DeletedAt  gorm.DeletedAt `json:"-" gorm:"index"`
// 	ItemID     uint           `json:"items"`
// 	PurchaseID uint           `json:"purchases"`
// }

type Purchase struct {
	Items      []Item      `json:"items"`
	Advantages interface{} `json:"advantages"`
}
