package model

import (
	"fmt"
	"time"

	"github.com/google/uuid"
)

// type ItemCart struct {
// 	ID        uint           `json:"id" gorm:"primaryKey;autoIncrement:true"`
// 	CreatedAt time.Time      `json:"created_at"`
// 	UpdatedAt time.Time      `json:"updated_at"`
// 	DeletedAt gorm.DeletedAt `json:"-" gorm:"index"`
// 	ItemID    uint
// 	CartID    uint
// }

// func (itemCart ItemCart) GetID() string {
// 	return fmt.Sprint(itemCart.ID)
// }

type CartItem struct {
	ID                 string `jsonapi:"primary,item_cart"`
	*Item              `jsonapi:"relation,item"`
	DiscountPercentage float32  `jsonapi:"attr,discount_percent"`
	Free               bool     `jsonapi:"attr,is_free"`
	FreeItems          []string `jsonapi:"attr,free_items"`
	Available          bool     `jsonapi:"attr,available"`
}

func (cartItem *CartItem) IsEqualSKU(sku string) bool {
	return cartItem.Item.SKU == sku
}

func (cartItem *CartItem) InSKU(sku []string) bool {
	for _, _sku := range sku {
		if _sku == cartItem.Item.SKU {
			return true
		}
	}

	return false
}

type Promotion struct {
	FreeItems []string `jsonapi:"attr,free_items" json:"free_items"`
	Discount  int      `jsonapi:"attr,discount" json:"discount"`
}

type MainPurchaseItem map[string]*Promotion
type PromoCampaign struct {
	SKU          []string         `json:"sku"`
	MainPurchase MainPurchaseItem `json:"MainPurchase"`
}

type PromoCampaigns map[string]PromoCampaign

func (pc *PromoCampaigns) GetAllSKUFromCampaign(name string) (affectedSKUs []string) {
	for campaignName, p := range *pc {
		if campaignName == name {
			affectedSKUs = p.SKU
		}
	}
	return
}

type Cart struct {
	ID                    string      `jsonapi:"primary,cart"`
	Items                 []*CartItem `jsonapi:"relation,items"`
	CheckedOutAt          *time.Time  `jsonapi:"attr,checked_out_at"`
	TotalPrice            float32     `jsonapi:"attr,total_price"`
	FinalPrice            int         `jsonapi:"attr,final_price"`
	Discount              int         `jsonapi:"attr,discount"`
	attachedRule          RuleEngine
	attachedPromoCampaign PromoCampaigns
}

func NewCart() *Cart {
	attachedRule := RuleEngine{
		Name:    "PromotionRules",
		Version: "1.0.0",
	}

	return &Cart{
		Items:        []*CartItem{},
		attachedRule: attachedRule,
	}
}

func (cart *Cart) AttachRules(rules Rules) {
	cart.attachedRule.AddRules(rules)
	cart.attachedRule.BuildEngine()
}

func (cart *Cart) AttachPromoCampaign(campaigns PromoCampaigns) {
	cart.attachedPromoCampaign = campaigns
}

func (cart *Cart) CalculatePotentialPromo() {
	for index, item := range cart.Items {
		cart.attachedRule.BuildEngine()
		cart.attachedRule.AddDataContext("Purchase", cart)
		cart.attachedRule.AddDataContext("PromoCampaign", cart.attachedPromoCampaign)
		cart.attachedRule.AddDataContext("index", index)
		cart.attachedRule.AddDataContext("Item", item)
		cart.attachedRule.Execute()
	}

}

func (cart *Cart) ItemWithSKUNo(sku string, UUID string, number int64) bool {
	var index int64 = 0
	var skuNo int64 = 0
	for _, cartItem := range cart.Items {
		if cartItem.Item.SKU == sku {
			index++

			if cartItem.Item.UUID == UUID {
				skuNo = index
			}
		}
	}

	return skuNo%number == 0
}

func (cart *Cart) RecalculateTotalPrice() {
	cart.TotalPrice = 0
	for _, cartItem := range cart.Items {

		if cartItem.Free {
			continue
		}

		if cartItem.DiscountPercentage > 0 {
			discount := (cartItem.Price * (cartItem.DiscountPercentage / 100))
			discountPrice := cartItem.Price - discount

			cart.TotalPrice += discountPrice
			continue
		}

		cart.TotalPrice += cartItem.Price
	}
}

func (cart *Cart) CheckItemsAvailibility(inventory *Inventory) {
	itemBySKUTotal := map[string]int{}

	for _, i := range *inventory {
		itemBySKUTotal[i.Item.SKU] = i.Quantity
	}

	for _, cartItem := range cart.Items {
		if itemBySKUTotal[cartItem.SKU] > 0 {
			cartItem.Available = true
			itemBySKUTotal[cartItem.SKU]--
		}
	}
}

func (cart *Cart) CheckFreeItemsInCart() {

	for _, cartItem := range cart.Items {
		fmt.Println(*cartItem)
		for index, skuOfFreeItem := range cartItem.FreeItems {
			for _, freeCartItem := range cart.Items {
				if freeCartItem.SKU == skuOfFreeItem {
					if len(cartItem.FreeItems) > 0 && !freeCartItem.Free {
						freeCartItem.Free = true
						cartItem.FreeItems = append(cartItem.FreeItems[:index], cartItem.FreeItems[index+1:]...)
					}
				}

			}
		}
	}

}

func (cart *Cart) AddItemToCart(item CartItem, quantity int) {
	for i := 0; i < quantity; i++ {
		newItemCart := item
		newItemCart.ID = uuid.NewString()
		cart.Items = append(cart.Items, &newItemCart)
	}
}

type ItemWithSKU map[string]*CartItem

func (itemWithSKU ItemWithSKU) Len() int {
	return len(itemWithSKU)
}

func (itemWithSKU *ItemWithSKU) ApplyDiscountPriceByPercent(percent float32) {
	for _, item := range *itemWithSKU {
		item.Price = item.Price - (item.Price * (percent / 100))
	}
}

func (cart *Cart) WithSKU(sku []string) int {
	foundItem := 0
	for _, item := range cart.Items {
		for _, _sku := range sku {
			if item.SKU == _sku {
				foundItem++
			}
		}
	}

	return foundItem
}
