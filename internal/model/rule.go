package model

import (
	"strings"

	"github.com/hyperjumptech/grule-rule-engine/ast"
	"github.com/hyperjumptech/grule-rule-engine/builder"
	"github.com/hyperjumptech/grule-rule-engine/engine"
	"github.com/hyperjumptech/grule-rule-engine/pkg"
)

type Rule struct {
	ID     string `jsonapi:"primary,rules"`
	Name   string `jsonapi:"attr,name"`
	Active bool   `jsonapi:"attr,is_active"`
	Raw    string `jsonapi:"attr,raw"`
}

type Rules []*Rule

func (rules *Rules) FindRuleByName(name string) *Rule {
	var foundRule *Rule
	for _, rule := range *rules {
		if rule.Name == name {
			foundRule = rule
		}
	}

	return foundRule
}

type RuleEngine struct {
	Name          string
	Version       string
	rules         string
	dataContext   ast.IDataContext
	knowledgeBase *ast.KnowledgeBase
	engine        *engine.GruleEngine
}

func (ruleEngine *RuleEngine) AddRules(rules Rules) {
	rulesToAdd := []string{}
	for _, rule := range rules {
		rulesToAdd = append(rulesToAdd, rule.Raw)
	}

	ruleEngine.rules = strings.Join(rulesToAdd, "\n\n")
}

func (ruleEngine *RuleEngine) BuildEngine() {
	knowledgeLibrary := ast.NewKnowledgeLibrary()
	ruleBuilder := builder.NewRuleBuilder(knowledgeLibrary)

	bs := pkg.NewBytesResource([]byte(ruleEngine.rules))
	err := ruleBuilder.BuildRuleFromResource(ruleEngine.Name, ruleEngine.Version, bs)
	if err != nil {
		panic(err)
	}

	ruleEngine.knowledgeBase = knowledgeLibrary.NewKnowledgeBaseInstance(ruleEngine.Name, ruleEngine.Version)
	ruleEngine.engine = engine.NewGruleEngine()
	ruleEngine.dataContext = ast.NewDataContext()
}

func (ruleEngine *RuleEngine) Execute() {
	err := ruleEngine.engine.Execute(ruleEngine.dataContext, ruleEngine.knowledgeBase)
	if err != nil {
		panic(err)
	}
}

func (ruleEngine *RuleEngine) AddDataContext(name string, fact interface{}) {
	err := ruleEngine.dataContext.Add(name, fact)
	if err != nil {
		panic(err)
	}
}
