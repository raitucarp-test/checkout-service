package model

import (
	"fmt"
	"os"

	"github.com/google/jsonapi"
)

type Item struct {
	UUID     string  `jsonapi:"primary,items"`
	SKU      string  `jsonapi:"attr,sku" json:"SKU"`
	Name     string  `jsonapi:"attr,name" json:"name"`
	Price    float32 `jsonapi:"attr,price" json:"price"`
	Currency string  `jsonapi:"attr,currency,omitempty" json:"currency,omitempty"`
}

func (item Item) JSONAPILinks() *jsonapi.Links {
	baseURL := os.Getenv("BASE_URL")
	return &jsonapi.Links{
		"self": fmt.Sprintf("%s/item/%s", baseURL, item.SKU),
	}
}

type Items []*Item

func (items *Items) FindItemBySKU(SKU string) *Item {
	var foundItem *Item

	for _, item := range *items {
		if item.SKU == SKU {
			foundItem = item
		}
	}

	return foundItem
}

func (items *Items) UpdateItemBySKU(SKU string, data *Item) *Item {
	foundItem := items.FindItemBySKU(SKU)
	if foundItem == nil {
		return foundItem
	}

	if &data.Price != nil {
		foundItem.Price = data.Price
	}

	if len(data.Name) > 0 {
		foundItem.Name = data.Name
	}

	return foundItem
}

func NewItem(SKU string, name string, price float32) *Item {
	return &Item{SKU: SKU, Name: name, Price: price, Currency: "$"}
}
