package data

import (
	"encoding/json"
	"fmt"
	"strings"

	"bitbucket.org/raitucarp-test/checkout-service/internal/model"
	"github.com/gobuffalo/packr"
	"github.com/google/uuid"
)

var Inventory model.Inventory
var Items model.Items = model.Items{}
var Rules model.Rules
var Cart *model.Cart = model.NewCart()
var initialInventoryQty []AddItemToInventoryRequestPayload = []AddItemToInventoryRequestPayload{}
var PromoCampaigns model.PromoCampaigns

func AddItem(item *model.Item) {
	Items = append(Items, item)
}

func PopulateFirstData() {
	ResetItemsAndInventory()
}

func AddAllInitialItemsToInventory() {
	for _, item := range Items {
		Inventory.AddItemToInventory(item, 0)
	}

	for _, inv := range initialInventoryQty {
		Inventory.UpdateItemQuantity(inv.SKU, inv.Quantity)
	}
}

func ResetItemsAndInventory() {
	LoadFixtures()

	fmt.Println(Items)
	Inventory = model.Inventory{}

	AddAllInitialItemsToInventory()
	AddInitialRules()
	Cart.AttachRules(Rules)
	Cart.AttachPromoCampaign(PromoCampaigns)
}

func LoadFixtures() {
	box := packr.NewBox("./../../fixtures")

	inventory, _ := box.FindString("inventory.json")
	if err := json.Unmarshal([]byte(inventory), &initialInventoryQty); err != nil {
		panic(err)
	}

	items, _ := box.FindString("items.json")
	loadedItems := model.Items{}
	if err := json.Unmarshal([]byte(items), &loadedItems); err != nil {
		panic(err)
	}

	for _, item := range loadedItems {
		item.UUID = uuid.NewString()
		Items = append(Items, item)
	}

	promoString, _ := box.FindString("promo.json")
	PromoCampaigns = model.PromoCampaigns{}
	if err := json.Unmarshal([]byte(promoString), &PromoCampaigns); err != nil {
		panic(err)
	}
}

func AddInitialRules() {
	box := packr.NewBox("./../../rules")

	for _, fileName := range box.List() {
		rule, err := box.FindString(fileName)
		if err != nil {
			panic(err)
		}

		Rules = append(Rules, &model.Rule{
			ID:     uuid.NewString(),
			Name:   strings.ReplaceAll(fileName, ".grl", ""),
			Raw:    rule,
			Active: true,
		})
	}

}
