package data

type AddItemToInventoryRequestPayload struct {
	SKU      string `json:"SKU"`
	Quantity int    `json:"quantity"`
}

type AddItemToCartRequestPayload struct {
	SKU      string `json:"SKU"`
	Quantity int    `json:"quantity"`
}

type RemoveItemInCartRequestPayload struct {
	Quantity int `json:"quantity"`
}
