package auth

import (
	"os"

	"github.com/gin-gonic/gin"
)

func CheckoutBasicAuth() gin.HandlerFunc {
	var accounts gin.Accounts = gin.Accounts{}
	username := os.Getenv("BASIC_AUTH_USERNAME")
	password := os.Getenv("BASIC_AUTH_PASSWORD")
	accounts[username] = password

	return gin.BasicAuth(accounts)
}
